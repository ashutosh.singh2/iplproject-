var matchData = require('./fileReader.js');


function matchesPerYear(matches, result) {
    let season = matches[1];
    let winnerTeam = matches[10];
    if (result.hasOwnProperty(season)) {
        result[season].hasOwnProperty(winnerTeam)?result[season][winnerTeam]++:result[season][winnerTeam] = 1;
        } else {
            result[season] = {};
            result[season][winnerTeam] = 1;

        }
        delete result['season'];
    }
async function execute() {
    let result = {};
    let filePath = "/home/ashutosh/projects/ipl-data/js-file/csv file/matches.csv"
    let p = await matchData.rowReader(filePath, matchesPerYear, result);
    console.log(p);

}
execute();