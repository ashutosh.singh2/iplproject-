var fs = require('fs');
var csv = require('fast-csv')

function matchesPerYear() {
  let matchesPerSeason = {};
  fs.createReadStream("./csv file/matches.csv")
    .pipe(csv.parse())
    .on('data', function (match) {
      let season = match[1];
      matchesPerSeason.hasOwnProperty(season)?matchesPerSeason[season]++:matchesPerSeason[season] = 1;
    })
    
    .on('end', function (data) {
      delete matchesPerSeason['season'];
      console.log(matchesPerSeason);
    });
}
matchesPerYear()