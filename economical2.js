var matchData = require('./fileReader.js');
const deliveryData = require("./matchid.js")

function economicalRun(matches, result, matchIdsPerSeason) {

    const matchId = +matches[0];
    const bowlerName = matches[8];
    const totalRuns = +matches[17];
    const wideRuns = matches[10]
    const noBall = matches[13];
    if (matchIdsPerSeason.includes(matchId)) {
        if (result.hasOwnProperty(bowlerName)) {
            result[bowlerName]['run'] += totalRuns;
            result[bowlerName]['bowl']++
        } else {
            result[bowlerName] = {}
            result[bowlerName]['run'] = totalRuns;
            result[bowlerName]['bowl'] = 1;

        }

    }
}

function asyncFinalResult(result) {
    return new Promise((resolve, reject) => {
        let avg = [];
        let arr = [];
        for (let bowler in result) {
            avg[bowler] = result[bowler]['run'] * 6 / result[bowler]['bowl'];
        }
        arr = Object.entries(avg);
        arr.sort((a, b) => {
            return a[1] - b[1]
        });
        resolve(arr);
    })

}

function finalResult(result) {
    let avg = {};
    let arr = [];
    for (let bowler in result) {
        avg[bowler] = result[bowler]['run'] * 6 / result[bowler]['bowl'];
    }
    arr = Object.entries(avg);
    arr.sort((a, b) => {
        return a[1] - b[1]
    });
    return arr;
}
async function execute() {
    let result = {};
    let array = [];
    array = await deliveryData.matchfileRead(2015)
    let deliveriesCsv = "/home/ashutosh/projects/ipl-data/js-file/csv file/deliveries.csv"
    let bowlerDetails = await matchData.rowReader(deliveriesCsv, economicalRun, result, array);
    // let economyRatesPerBowler = await asyncFinalResult(bowlerDetails);
    let economyRatesPerBowler = finalResult(bowlerDetails)
    console.log(economyRatesPerBowler);

}
execute();