var matchData = require('./fileReader.js');
const deliveryData = require("./matchid.js")

function batsmanRuntotal(matches, result, arr) {
    let matchId = +matches[0]
    let batsmanName = matches[6];
    let batsmanRun = +matches[15];
    let wide=+matches[10];
    

    if (arr.includes(matchId)) {
        if (batsmanName in result) {
            result[batsmanName]['runs'] += batsmanRun;   
            if(wide==0){
            result[batsmanName]['balls']+=1
            }
        } else {
            result[batsmanName] = {};
            result[batsmanName]['runs']=batsmanRun
            result[batsmanName]['balls']=1
        }
    }
    
}

function batsmanRunResult(result) { 
    let strikeRate = {};
    let arr = [];
    for (let batsman in result) {
        if(result[batsman]['runs']>200){
        strikeRate[batsman] = (result[batsman]['runs'] * 100)/( result[batsman]['balls']);
        }
    }

    arr=Object.entries(strikeRate);
    
    arr= arr.sort((a,b)=>{
        return b[1]-a[1]
    })
    return arr;

}
async function execute() {
    let result = {};
    let array = [];
    array = await deliveryData.matchfileRead(2013)
    let filePath = "/home/ashutosh/projects/ipl-data/js-file/csv file/deliveries.csv"
    let batsmenDetails = await matchData.rowReader(filePath, batsmanRuntotal, result, array);
    let topStrikeRates = batsmanRunResult(batsmenDetails);
    console.log(topStrikeRates);
}
execute();