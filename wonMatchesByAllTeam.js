var fs = require('fs');
var csv = require('fast-csv')

function matchesWonByTEamYear() {
    let matchesWonPerSeasonPerTeam = {};
    fs.createReadStream("./csv file/matches.csv")
        .pipe(csv.parse())
        .on('data', function (match) {
            let season = match[1];
            let team = match[10];
            if (matchesWonPerSeasonPerTeam.hasOwnProperty(season)) {
            matchesWonPerSeasonPerTeam[season].hasOwnProperty(team)?matchesWonPerSeasonPerTeam[season][team]++:matchesWonPerSeasonPerTeam[season][team] = 1;
            } else {
                matchesWonPerSeasonPerTeam[season] = {};
                matchesWonPerSeasonPerTeam[season][team] = 1;

            }
        })
        .on('end', function (finalREsult) {
            delete matchesWonPerSeasonPerTeam['season'];
            console.log(matchesWonPerSeasonPerTeam);
        });
}
matchesWonByTEamYear();