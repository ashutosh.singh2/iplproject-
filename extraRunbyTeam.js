var fs = require('fs');
var csv = require('fast-csv')
const matchData = require('./matchid.js')

function deliveriesFileRead(reqSessionId) {
    let extraRunByTeam = {};
    fs.createReadStream("./csv file/deliveries.csv")
        .pipe(csv.parse())
        .on('data', function (delivery) {
            
            let matchId = delivery[0];
            let bowlingTeam = delivery[3];
            let extaRun = delivery[16];
            if (matchId >= reqSessionId[0] && matchId <= reqSessionId[reqSessionId.length - 1]) {
                if (extraRunByTeam.hasOwnProperty(bowlingTeam)) {
                    extraRunByTeam[ bowlingTeam] += parseInt(extaRun);
                } else {
                    extraRunByTeam[ bowlingTeam] = parseInt(extaRun);
                }

            }
        })
        .on('end', function (endingData) {
            
            console.log(extraRunByTeam);
        });
}
async function runningProgram() {
    let reqSessionId = [];
    reqSessionId = await matchData.matchfileRead(2016);
    deliveriesFileRead(reqSessionId);
}
runningProgram();