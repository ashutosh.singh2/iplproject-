var fs = require('fs');
var csv = require('fast-csv')
const matchData = require('./matchid.js')

function deliveriesFileRead(requiredSessionId) {
    let bowlingStats = {};
    fs.createReadStream("./csv file/deliveries.csv")
        .pipe(csv.parse())
        .on('data', function (dilevery) {
            let matchId = dilevery[0];
            let bowlerName = dilevery[8];
            let totalRun = dilevery[17];
            if (requiredSessionId[0] <= (matchId) && (matchId <= requiredSessionId[requiredSessionId.length - 1])) {

                if (bowlingStats.hasOwnProperty(bowlerName)) {
                    bowlingStats[bowlerName]['total'] += parseInt(totalRun);
                    if (dilevery[10] != 0 && dilevery[13] != 0) {} else {
                        bowlingStats[bowlerName]['bowl']++
                    }
                } else {
                    bowlingStats[bowlerName] = {}
                    bowlingStats[bowlerName]['total'] = parseInt(totalRun);
                    dilevery[10] != 0 && dilevery[13] != 0 ? bowlingStats[bowlerName]['bowl'] = 0 : bowlingStats[bowlerName]['bowl'] = 1
                }
            }

        })
        .on('end', function (dilevery) {

            for (var x in bowlingStats) {
                bowlingStats[x]['eco'] = ((bowlingStats[x]['total'] / bowlingStats[x]['bowl']) * 6).toFixed(4);
            }
            var averageList = [];
            for (var x in bowlingStats) {
                var bolwerlist = [];
                bolwerlist.push(x);
                bolwerlist.push(bowlingStats[x]['eco']);
                averageList.push(bolwerlist);


            }
            averageList.sort((a, b) => {
                return a[1] - b[1]
            });
            let econmyBolwer = {};
            for (var i = 0; i < 10; i++) {
                econmyBolwer[averageList[i][0]] = averageList[i][1];
            }
            console.log(econmyBolwer)
        });
}


async function runningProgram() {
    let reqSessionId = [];
    reqSessionId = await matchData.matchfileRead(2015);
    //  console.log(reqSessionId);
    deliveriesFileRead(reqSessionId);
}
runningProgram();