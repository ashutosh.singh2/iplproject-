var fs = require('fs');
var csv = require('fast-csv')


function rowReader(csvFile, callbackFunction, result,arr) {
    
    return new Promise((resolve, reject) => {
        fs.createReadStream(csvFile)
            .pipe(csv.parse())
            .on('data', function (match) {
                callbackFunction(match, result,arr)
            })
            .on('end', function () {
                resolve(result)
            })
    })
}
module.exports.rowReader=rowReader;

// function calculate(match, result) {
//     console.log(match, result)
// }
// async function execute() {
//     let result = {}
//     let filePath = "/home/ashutosh/projects/ipl-data/js-file/csv file/matches.csv"
//     let p = await rowReader(filePath, calculate, result);
//     console.log(p)
// }
// execute();