var fs = require('fs');
var csv = require('fast-csv')


function matchfileRead(sessionReq) {
    return new Promise((res, rej) => {
        let sessionId = [];
        fs.createReadStream("./csv file/matches.csv")
            .pipe(csv.parse())
            .on('data', function (data) {
                let Id = data[0];
                let session = data[1];
                if (session == sessionReq) {
                    sessionId.push(parseInt(Id));
                }
            })
            .on('end', function (data) {


                res(sessionId);
            });

    });


}

// let array=[];
// async function call(){
//  array= await matchfileRead(2015);
//  console.log(array)
// }
// call();
exports.matchfileRead = matchfileRead;